package main;
import java.util.ArrayList;

import model.BankAccount;
import model.Company;
import model.Country;
import model.Data;
import model.Measurable;
import model.Person;
import model.Product;
import model.Taxable;
import model.TexCalculator;


public class Measurable_Taxalbe_testcase {
	public static void main(String[] args){
		new Measurable_Taxalbe_testcase();
	}
	public Measurable_Taxalbe_testcase(){
		testMeasurable();
		testTaxable();
	}
	public void testMeasurable(){
		Person s1 = new Person("Black",175);
		Person s2 = new Person("Wut",171.5);
		Person s3 = new Person("Pond",180);
		
		Measurable[] mea = new Measurable[3];
		mea[0] = s1;
		mea[1] = s2;
		mea[2] = s3;
		
		System.out.println("\t\tMeasurable");
		System.out.println(s1.getPerson());
		System.out.println(s2.getPerson());
		System.out.println(s3.getPerson());
		System.out.println("Test Average : "+Data.average(mea));
		
		System.out.println("\n---Person---");
		System.out.println(s1.getPerson());
		System.out.println(s2.getPerson());
		Person s = (Person)Data.min(s1,s2);
		System.out.println("Shortter is : "+s.getPerson());
		
		BankAccount b1 = new BankAccount("May",5000);
		BankAccount b2 = new BankAccount("Kik",4800);
		BankAccount b = (BankAccount)Data.min(b1, b2);
		System.out.println("---BankAccount---");
		System.out.println(b1.getBankAccount());
		System.out.println(b2.getBankAccount());
		System.out.println("Balance less is : "+b.getBankAccount());
		
		Country c1 = new Country("PondSweet",555);
		Country c2 = new Country("YimJubu",345);
		Country c = (Country)Data.min(c1, c2);
		System.out.println("---Country---");
		System.out.println(c1.getCountry());
		System.out.println(c2.getCountry());
		System.out.println("Area less is "+c.getCountry());
	}
	
	public void testTaxable(){
		System.out.println("\n\t\tTaxble");
		ArrayList<Taxable> person = new ArrayList<Taxable>();
		person.add(new Person("Black",500000));
		person.add(new Person("Wut",100000));
		person.add(new Person("Pond",300000));
		System.out.println("All sum person : "+TexCalculator.sum(person)+" bath");
		
		ArrayList<Taxable> company = new ArrayList<Taxable>();
		company.add(new Company("Keen",100000,80000));
		company.add(new Company("Mark",100000,150000));
		company.add(new Company("June",80000,55000));
		System.out.println("All sum company : "+TexCalculator.sum(company)+" bath");
		
		ArrayList<Taxable> product = new ArrayList<Taxable>();
		product.add(new Product("Book",350));
		product.add(new Product("Computer",21000));
		product.add(new Product("Pecil",70));
		System.out.println("All sum product : "+TexCalculator.sum(product)+" bath");
		
		ArrayList<Taxable> all = new ArrayList<Taxable>();
		all.add(new Person("Black",500000));
		all.add(new Company("Keen",100000,80000));
		all.add(new Product("Book",150));
		System.out.println("All sum person,company,product : "+TexCalculator.sum(all)+" bath");
	}
}
