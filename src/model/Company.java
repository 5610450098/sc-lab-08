package model;

public class Company implements Taxable{
	private String name;
	private double income;
	private double expense;
	
	public Company(String name,double income,double expense){
		this.name = name;
		this.income = income;
		this.expense = expense;
	}
	
	public double getTax() {
		if(expense>income){
			return 0;
		}
		else{
			return (income-expense)*0.3;
		}
	}

}
