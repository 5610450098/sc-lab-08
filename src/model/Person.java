package model;

public class Person implements Measurable,Taxable{
	private String name;
	private double height_salary;
	public Person(String name,double height_salary){
		this.name = name;
		this.height_salary = height_salary;
	}
	public double getMeasurable() {
		return height_salary;
	}
	public String getPerson(){
		return name+" "+height_salary+" c.m.";
	}
	
	public double getTax() {
		if(height_salary<=300000){
			return height_salary*0.05;
		}
		else{
			return (height_salary-300000)*0.1+15000;
		}
	}

}
