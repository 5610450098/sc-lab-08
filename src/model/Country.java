package model;

public class Country implements Measurable{
	private String name;
	private double area;
	public Country(String name,double area){
		this.name = name;
		this.area = area;
	}
	public double getMeasurable() {
		return area;
	}
	public String getCountry(){
		return name+" "+area+" area";
	}

}
