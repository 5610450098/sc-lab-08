package model;

public interface Taxable {
	double getTax();
}
