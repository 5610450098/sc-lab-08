package model;

public interface Measurable {
	double getMeasurable();
}
