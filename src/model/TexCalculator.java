package model;
import java.util.ArrayList;


public class TexCalculator {
	public static double sum(ArrayList<Taxable> taxList){
		double tax = 0;
		for(Taxable t : taxList){
			tax += t.getTax();
		}
		return tax;
	}
}
