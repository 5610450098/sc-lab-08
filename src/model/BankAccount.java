package model;

public class BankAccount implements Measurable{
	private String name;
	private double balance;
	public BankAccount(String name,double balance){
		this.name = name;
		this.balance = balance;
	}
	public double getMeasurable() {
		return balance;
	}
	public String getBankAccount(){
		return name+" "+balance+" bath";
	}
}
