package model;

public class Product implements Taxable{
	private String name;
	private double price;
	
	public Product(String name,double price){
		this.name = name;
		this.price = price;
	}
	
	public double getTax() {
		return price*7/100;
	} 

}
