package model;

public class Data{
	
	public static double average(Measurable[] mea){
		double avg = 0;
		for(Measurable height : mea){
			avg += height.getMeasurable();
		}
		if(mea.length<0){
			return 0;
		}
		else{
			return avg/mea.length;
		}
	}
	
	public static Measurable min(Measurable m1,Measurable m2){
		if(m1.getMeasurable()>m2.getMeasurable()){
			return m2;
		}
		else {
			return m1;
		}
	}
	
	

}
